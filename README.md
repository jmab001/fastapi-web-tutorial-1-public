# FastAPI Web Tutorial 1
```bash
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt


docker build -t fastapiplaywebsite1 .
docker run -d --name fastapiplaywebsite1 -p 80:80 fastapiplaywebsite1
```
Visit Swagger API pages:  
    [View default](http://127.0.0.1/)  
    [View in localhost](http://127.0.0.1/items/5?q=somequery)  
    should see this:  
    ```JSON
    {"item_id": 5, "q": "somequery"}
    ```  
    [See the docs on localhost](http://127.0.0.1/docs)
    [See the alternative docs on localhost]( http://127.0.0.1/redoc)
